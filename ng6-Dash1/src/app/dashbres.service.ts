import { Injectable} from '@angular/core';
import { LoginService } from '../app/login.service';

import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs';
import {LogBarData} from '../app/log-bar-data';

@Injectable()
export class Dashbresolver implements Resolve<LogBarData[]> {

    constructor(private loginservice: LoginService ) {}

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ) : Observable<LogBarData[]> {
        return this.loginservice.Login(this.loginservice.strTmpUserName);
    }
}
