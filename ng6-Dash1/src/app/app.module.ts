import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatGridListModule, MatCardModule, MatMenuModule, MatIconModule, MatButtonModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { MydashboardComponent } from './mydashboard/mydashboard.component';
import { Ng6O2ChartModule } from 'ng6-o2-chart';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { Dashbresolver } from './dashbres.service';
import { LoginService } from './login.service';


const appRoutes: Routes = [
  {path: '', component: LoginComponent, data: { title: 'Sales Person DashBoard' }},
  {path: 'Dashboard', component: MydashboardComponent, resolve : { 'LogBarData[]': Dashbresolver }},
  {path: 'Login', component: LoginComponent, data: { title: 'Sales Person DashBoard' }},
];

@NgModule({
  declarations: [
    AppComponent,
    MydashboardComponent,
    LoginComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule,
    Ng6O2ChartModule
  ],
  providers: [  LoginService, Dashbresolver],
  bootstrap: [AppComponent]
})
export class AppModule { }
