import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {from, Observable, pipe} from 'rxjs';
import {map } from 'rxjs/operators';
import {LogBarData} from '../app/log-bar-data';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  //convert this into an array
  test: LogBarData;
  public lgdataarray: LogBarData[] = [] ;
  public lgdataarray32: LogBarData[] = [] ;
  series = [];
  data = [];
  num = [];
  i: number;
 strTmpseries:   string ;
 strTmpdata:   string ;
 strTmpUserName:   string ;
 strDataarray: string[] = [];
 strDataarray1: string[] = [];
 tempdata: { x: string; y: number[]; } [] = [];
 tempnum: number[] = [];
 tempstrX : string ;
 


 // lgdataarray1: any = [];
 public logbarDataJson: LogBarData ;//= new LogBarData();
 // logbarDataJson1: LogBarData = new LogBarData();
 /* {
    'series': [
      'SalesPerson',
      'Total'
    ],
    'data': [
      {
        'x': 'Furniture',
        'y': [92, 73],
      },
      {
        'x': 'Bedding',
        'y': [69, 45],
      },
      {
        'x': 'Warranty',
        'y': [70, 100],
      },
      {
        'x': 'Access',
        'y': [43, 66],
      },
      {
        'x': 'Delivery',
        'y': [60, 70],
      },
    ],
  };
*/
 public  logbarDataJson1:  LogBarData ;//= new LogBarData();
 /*{
    'series': [
      'SalesPerson',
      'Total'
    ],
    'data': [
      {
        'x': 'Mon',
        'y': [92, 73],
      },
      {
        'x': 'Tue',
        'y': [69, 45],
      },
      {
        'x': 'Wed',
        'y': [70, 100],
      },
      {
        'x': 'Thu',
        'y': [43, 66],
      },
      {
        'x': 'Fri',
        'y': [60, 70],
      },
      {
        'x': 'Sat',
        'y': [55, 63],
      },
      {
        'x': 'Sun',
        'y': [45, 93],
      },
    ],
  };
*/
  constructor(private http : HttpClient) { 

   // this.logbarDataJson = new LogBarData();
   }

  Login(userID:  string): Observable<LogBarData[]> {
    this.i = 0;
    return this.http.get<LogBarData[]>('http://localhost:58330/api/values/', {

      params: {
 
        id: `${userID}`
 
      }    } )   .pipe (
      map((data : LogBarData[]) => {
        this.lgdataarray = data ;//Object.assign([], data) ;
        console.log(JSON.stringify(data), 'the data response data');
        console.log(this.lgdataarray, 'the data response');
      //  this.lgdataarray.forEach(y => { //this.lgdataarray.push(y);
      data.forEach(y =>{
        if (this.i === 0)
        {
          console.log(y.data, 'y.data');
          console.log(y.data.length, 'y.data length');
          this.logbarDataJson = new LogBarData(y.series, (y.data));
          this.i++;



       // console.log(this.logbarDataJson.data, 'data');
       // console.log(this.logbarDataJson.series, 'series');
        }
        else
        {
       //   this.logbarDataJson1.data.push(data.indexOf[1].data);
        //  this.logbarDataJson1.series.push(data.indexOf[1].series);
         this.logbarDataJson1 = new LogBarData(y.series, (y.data));
        }
      // this.series.push(y.series);
       });
     //   this.logbarDataJson.data.push(this.lgdataarray[0].data) ;//= this.lgdataarray[0];
     //   this.lgdataarray.forEach (x => {})
       // this.logbarDataJson1 = this.lgdataarray[1];
  //      console.log(this.logbarDataJson);
       // console.log(this.logbarDataJson1);
      return data ; }));


  }
}
